# ECE5554 Computer Vision: Enhanced Drive Lane Detection and Segmentation Using Computer Vision Techniques.

This project focuses on the detection and segmentation of driving lanes using computer vision techniques. The project uses the KITTI dataset for evaluation. 

## Getting started

### Prerequisites

- Python 3.x
- Required Python libraries: numpy, opencv-python, matplotlib, scikit-learn (You can install these using pip: `pip install numpy opencv-python matplotlib scikit-learn`)

The first thing you have to do is to download the KITTI dataset. You can download it from [here](http://www.cvlibs.net/datasets/kitti/eval_object.php?obj_benchmark=3d). You will need the raw images, OXTS, and velodyne points.

```bash
cd dataset
./downloadKITTI.sh
```

The second thing you have to do is to download the TU Simple Lane dataset. You can download it from [here](https://www.kaggle.com/datasets/manideep1108/tusimple/). You will need the training and testing images. (You can install kaggle using pip: `pip install kaggle`)

```bash
cd dataset
./downloadTUSimple.sh
```

The third thing to run the geodata code for the satelite images. You will need a google API key and add a environment file to the notebook. Contact the author for the google maps API key.

```bash
GOOGLE_MAPS_API_KEY =
```
## Running the code

The quickest way is to run the QuickFit.py file. This will run the curve-fitting method on the user defined dataset. The user can define the dataset path and the output path with command line arguments. The program will output CSV files with the F1 score for each image in the dataset. The program will also display the progress and runtime performance in the terminal.

![Alt text](quickfit_app.png)

```bash
# Usage: python QuickFit.py <dataset_path> <label_path> <output_path>
python3 QuickFit.py ../dataset/TUSimple/train_set/clips/0531 ../dataset/TUSimple/train_set/label_data_0531.json ../output/
```

### Notebooks

The notebooks folder contains the following notebooks:
- analysis.ipynb: This notebook contains the analysis of the results. It also contains the code to render the RANSAC and Lane Prediction outputs.
- nonCNNapproach.ipynb: This notebook contains the code for the initial nonCNN approach. It does all the processing as QuickFit.py but it does it all in one notebook.
- geodata_notebook.ipynb: This notebook contains the code for the geodata approach. It uses QuickFit.py to run the curve-fitting method on the geodata images.
- CNNpedestrian.ipynb: This notebook contains some exploration code developed by Shilong Zong. It uses a CNN to detect pedestrians in the images.

## Background

We use a combination of computer vision techniques to detect and segment driving lanes. We have established 4 main state-of-the art methods that can be used to detect and segment driving lanes. These methods are:

- Segmentation Approach
- Row-wise classification approach
- Curve-figging approach
- Anchor-based approach

Quickfitting is a method that can be used to fit a curve to a set of points. We use this method to fit a curve to the detected lane points. We use the KITTI dataset to evaluate our sattellite enhanced methods. We use the TU Simple Lane dataset to evaulate our curve-fitting method.

The used evaluation metric is the F1 score. The F1 score is the harmonic mean of the precision and recall. In the lane detection problem a F1 standard is a pixel-wise comparrisson with a small acceptable pixel deviation of 40. The precision is the ratio of the true positives to the sum of the true positives and false positives. The recall is the ratio of the true positives to the sum of t. Here is an example of a test run with F1 scores of the curve-fitting method:

![histbox](histbox_plot.png)

## Results

The RANSAC output and the Lane Prediction output can be rendered using the QuickFit functions. See the analysis.ipynb notebook for an example of how to use the QuickFit functions. Here is an example of the RANSAC and Lane Prediction outputs:

![ransac](best_ransac_5.png)
![image](best.png)

## Authors
- Timo Thans - ECE, Virginia Tech


