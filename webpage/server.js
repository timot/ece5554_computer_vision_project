// server.js
const express = require('express');
const fs = require('fs');
const path = require('path');

const app = express();

// Serve static files from the "public" directory
app.use(express.static(path.join(__dirname, 'public')));

function serveImages(folder, endpoint) {
    app.get(endpoint, (req, res) => {
        const imagesDir = path.join(__dirname, 'public/results/', folder);

        fs.readdir(imagesDir, (err, files) => {
            if (err) {
                console.error(err);
                res.status(500).send('Server error');
                return;
            }

            const images = files.map(file => `/results/${folder}/${file}`);
            res.json(images);
        });
    });
}

// Serve images from the "images" folder at the "/teaser" endpoint
serveImages('teaser', '/teaser');

// Serve images from the "ransac" folder at the "/ransac" endpoint
serveImages('ransac', '/ransac');

// Serve images from the "failures" folder at the "/failures" endpoint
serveImages('failures', '/failures');

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});