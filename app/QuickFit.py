import sys
from sklearn.linear_model import RANSACRegressor
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import asyncio
from concurrent.futures import ProcessPoolExecutor
import json
import glob
import numpy as np
import cv2
import time

"""
QuickFit is a quick and easy way to do lane detection on frames from dashcams or other car cameras. It works by splitting the image into a left and right half, then using RANSAC to fit a line to the points in each half. 

Usage: python QuickFit.py <path_to_images> <path_to_labels>
"""

class QuickFit():
    
    """
    QuickFit class for lane detection and fitting.

    Parameters:
    - gaussian_blur (int): Size of the Gaussian blur kernel. Default is 5.
    - canny_low (int): Lower threshold for Canny edge detection. Default is 50.
    - canny_high (int): Upper threshold for Canny edge detection. Default is 150.
    - ransac_min_samples (int): Minimum number of samples required to fit a line using RANSAC. Default is 5.
    - ransac_residual_threshold (float): Maximum residual distance for a data point to be considered an inlier in RANSAC. Default is 5.0.
    - ransac_max_trials (int): Maximum number of iterations for RANSAC. Default is 1000.
    - ransac_stop_probability (float): Probability to stop RANSAC early if the best model is found. Default is 0.99.
    - ransac_random_state (int or None): Random seed for RANSAC. Default is None.
    - polynomial_degree (int): Degree of the polynomial used for fitting the lane line. Default is 3.
    - f1_threshold (int): Threshold for F1 score computation. Default is 40.
    """

    def __init__(self, gaussian_blur=5, canny_low=50, canny_high=150, ransac_min_samples=5, ransac_residual_threshold=5.0, ransac_max_trials=1000, ransac_stop_probability=0.99, ransac_random_state=None, polynomial_degree=3, f1_threshold=40):
        self.gaussian_blur = gaussian_blur
        self.canny_low = canny_low
        self.canny_high = canny_high
        self.ransac_min_samples = ransac_min_samples
        self.ransac_residual_threshold = ransac_residual_threshold
        self.ransac_max_trials = ransac_max_trials
        self.ransac_stop_probability = ransac_stop_probability
        self.ransac_random_state = ransac_random_state
        self.polynomial_degree = polynomial_degree
        self.f1_threshold = f1_threshold
        
    @staticmethod
    def load_labels(file, clip):
        """
        Loads the labels from the given path. 
        """
        with open(file) as f:
            # Read Line by Line and convert to json
            for line in f.readlines():
                data = json.loads(line)
                # Find the raw file name
                if data['raw_file'] == clip:
                    break
        lanes = data['lanes']
        ysamples = data['h_samples']
        lanes = [[(x, y) for (x, y) in zip(lane, ysamples) if x >= 0] for lane in lanes]
        return lanes
    
    @staticmethod
    def load_image(path):
        """
        Loads the image from the given path. 
        """
        image = cv2.imread(path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        return image
    
    def apply_filter(self, image):
        """
        Applies a filter to the image. 
        """
        # Convert the image to grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        # Apply a Gaussian blur to the image
        blur = cv2.GaussianBlur(gray, (self.gaussian_blur, self.gaussian_blur), 0)
        # Apply Canny edge detection to the image
        edges = cv2.Canny(blur, self.canny_low, self.canny_high)
        # Apply a mask to the image
        mask = np.zeros_like(gray)
        ignore_mask_color = 255
        imshape = image.shape
        vertices = np.array([[(0,imshape[0]),(450, 220), (800, 220), (imshape[1], imshape[0])]], dtype=np.int32)
        cv2.fillPoly(mask, vertices, ignore_mask_color)
        edges = cv2.bitwise_and(edges, mask)
        # Get all the pixels that are not black
        pixels = np.where(edges == 255)
        coords = np.array([(x, y) for (x, y) in zip(pixels[1], pixels[0])])
        return coords

    @staticmethod
    def split_pixels(coords, image):
        """
        Splits the pixels into left and right halves. 
        """
        midpoint = image.shape[1] / 2
        left_coords = coords[coords[:,0] < midpoint]
        right_coords = coords[coords[:,0] >= midpoint]
        return left_coords, right_coords

    def fit_line(self, pixels):
        """
        Fits a line to the given pixels. 
        """
        # Flatten the pixels
        x_pixels = np.array([coord[0] for coord in pixels])
        y_pixels = np.array([coord[1] for coord in pixels])
        # Create a RANSAC model
        self.ransac = RANSACRegressor(min_samples=self.ransac_min_samples, residual_threshold=self.ransac_residual_threshold, max_trials=self.ransac_max_trials, random_state=self.ransac_random_state, stop_probability=self.ransac_stop_probability)
        # Create a pipeline
        model = make_pipeline(PolynomialFeatures(self.polynomial_degree), self.ransac)
        # Fit the model to the pixels
        model.fit(np.expand_dims(x_pixels, axis=1), y_pixels)
        return model

    @staticmethod
    def predict_line(model, x_range, n=100):
        """
        Predicts the line for the given pixels. 
        """
        x_vals = np.linspace(x_range[0], x_range[1], n)
        y_hat = model.predict(np.expand_dims(x_vals, axis=1))
        # Return the line
        return [(x, y) for (x, y) in zip(x_vals, y_hat)]
    
    @staticmethod
    def get_best_MSE(lanes, model):
        """
        Returns the lane, and y hat with the lowest MSE. 
        """
        mse = 10000
        best_y_hat, best_lane = [], []
        for lane in lanes:
            if len(lane) < 2:
                continue
            y_hat = model.predict(np.expand_dims(np.array([coord[0] for coord in lane]), axis=1))
            y_lane = np.array([coord[1] for coord in lane])
            if np.mean(np.abs(y_lane - np.array(y_hat))) < mse:
                mse = np.mean(np.abs(y_lane - np.array(y_hat)))
                best_y_hat = y_hat
                best_lane = lane
        best_xy_hat = [(x, y) for (x, y) in zip(np.array([coord[0] for coord in best_lane]), best_y_hat)]
        return best_lane, best_xy_hat
            
    def compute_f1_score(self, pred_coords, label_coords, image):
        """
        Computes the F1 score using pixel-wise comparison.
        """
        assert len(pred_coords) == len(label_coords)
        # Flatten the predicted lane and label
        x_coords = np.array([coord[0] for coord in pred_coords])
        y_hat_coords = np.array([coord[1] for coord in pred_coords])
        y_coords = np.array([coord[1] for coord in label_coords])
        # Draw the predicted lane and label
        predicted = np.zeros_like(image)
        gt_img = np.zeros_like(image)
        predicted_wide = np.zeros_like(image)
        gt_img_wide = np.zeros_like(image)
        for i in range(len(x_coords) - 1):
            gt_img_wide = cv2.line(gt_img_wide, (int(x_coords[i]), int(y_coords[i])), (int(x_coords[i + 1]), int(y_coords[i + 1])), (255, 255, 255), self.f1_threshold)
            gt_img = cv2.line(gt_img, (int(x_coords[i]), int(y_coords[i])), (int(x_coords[i + 1]), int(y_coords[i + 1])), (255, 255, 255), 1)
            predicted = cv2.line(predicted, (int(x_coords[i]), int(y_hat_coords[i])), (int(x_coords[i + 1]), int(y_hat_coords[i + 1])), (255, 255, 255), 1)
            predicted_wide = cv2.line(predicted_wide, (int(x_coords[i]), int(y_hat_coords[i])), (int(x_coords[i + 1]), int(y_hat_coords[i + 1])), (255, 255, 255), self.f1_threshold)    
        # Compute the F1 score
        tp = np.sum(np.logical_and(predicted == 255, gt_img_wide == 255))
        fp = np.sum(np.logical_and(predicted == 255, gt_img_wide == 0))
        fn = np.sum(np.logical_and(predicted_wide == 0, gt_img == 255))
        accuracy = tp / (tp + fp + fn)
        return accuracy, fp, fn
    
    @staticmethod
    def draw_lines(image, left_line, left_pred_line, right_line, right_pred_line):
        """
        Draws the lines on the image. 
        """
        assert len(left_line) == len(left_pred_line)
        assert len(right_line) == len(right_pred_line)
        x_coords = np.array([coord[0] for coord in left_line])
        y_hat_coords = np.array([coord[1] for coord in left_pred_line])
        y_coords = np.array([coord[1] for coord in left_line])
        for i in range(len(left_line)-1):
            cv2.line(image, (int(x_coords[i]), int(y_hat_coords[i])), (int(x_coords[i + 1]), int(y_hat_coords[i + 1])), (255, 127, 127), 3)
            cv2.line(image, (int(x_coords[i]), int(y_coords[i])), (int(x_coords[i + 1]), int(y_coords[i + 1])), (144, 238, 144), 3)
        x_coords = np.array([coord[0] for coord in right_line])
        y_hat_coords = np.array([coord[1] for coord in right_pred_line])
        y_coords = np.array([coord[1] for coord in right_line])
        for i in range(len(right_line)-1):
            cv2.line(image, (int(x_coords[i]), int(y_hat_coords[i])), (int(x_coords[i + 1]), int(y_hat_coords[i + 1])), (255, 127, 127), 3)
            cv2.line(image, (int(x_coords[i]), int(y_coords[i])), (int(x_coords[i + 1]), int(y_coords[i + 1])), (144, 238, 144), 3)
        fig, ax = plt.subplots(1, 1, figsize=(16, 5))
        ax.imshow(image)
        ax.axis('off')
        return fig
    
    @staticmethod
    def draw_pred_lines(image, left_pred_line, right_pred_line):
        """
        Draws the lines on the image. 
        """
        x_coords = np.array([coord[0] for coord in left_pred_line])
        y_hat_coords = np.array([coord[1] for coord in left_pred_line])
        for i in range(len(left_pred_line)-1):
            cv2.line(image, (int(x_coords[i]), int(y_hat_coords[i])), (int(x_coords[i + 1]), int(y_hat_coords[i + 1])), (255, 127, 127), 3)
        x_coords = np.array([coord[0] for coord in right_pred_line])
        y_hat_coords = np.array([coord[1] for coord in right_pred_line])
        for i in range(len(right_pred_line)-1):
            cv2.line(image, (int(x_coords[i]), int(y_hat_coords[i])), (int(x_coords[i + 1]), int(y_hat_coords[i + 1])), (255, 127, 127), 3)

        fig, ax = plt.subplots(1, 1, figsize=(16, 5))
        ax.imshow(image)
        ax.axis('off')
        return fig
    
    def render_LR_F1(self, left_pred_line, right_pred_line, left_labels, right_labels, img_shape):
        """
        Renders the left and right F1 score in one image.
        """
        # Check prediction and label coordinates are the same shape
        assert len(left_pred_line) == len(left_labels)
        # Draw the predicted lane and label
        left_predicted = np.zeros_like(img_shape)
        left_gt_img = np.zeros_like(img_shape)
        left_predicted_wide = np.zeros_like(img_shape)
        left_gt_img_wide = np.zeros_like(img_shape)
        # x and y coordinates for the predicted lane
        left_x_coords = np.array([coord[0] for coord in left_pred_line])
        left_y_hat_coords = np.array([coord[1] for coord in left_pred_line])
        left_y_coords = np.array([coord[1] for coord in left_labels])
        for i in range(len(left_x_coords) - 1):
            left_gt_img_wide = cv2.line(left_gt_img_wide, (int(left_x_coords[i]), int(left_y_coords[i])), (int(left_x_coords[i + 1]), int(left_y_coords[i + 1])), (255, 255, 255), self.f1_threshold)
            left_gt_img = cv2.line(left_gt_img, (int(left_x_coords[i]), int(left_y_coords[i])), (int(left_x_coords[i + 1]), int(left_y_coords[i + 1])), (255, 255, 255), 1)
            predicted = cv2.line(left_predicted, (int(left_x_coords[i]), int(left_y_hat_coords[i])), (int(left_x_coords[i + 1]), int(left_y_hat_coords[i + 1])), (255, 255, 255), 1)
            left_predicted_wide = cv2.line(left_predicted_wide, (int(left_x_coords[i]), int(left_y_hat_coords[i])), (int(left_x_coords[i + 1]), int(left_y_hat_coords[i + 1])), (255, 255, 255), self.f1_threshold)
        
        # Check prediction and label coordinates are the same shape
        assert len(right_pred_line) == len(right_labels)
        
        # Draw the predicted lane and label
        right_predicted = np.zeros_like(img_shape)
        right_gt_img = np.zeros_like(img_shape)
        right_predicted_wide = np.zeros_like(img_shape)
        right_gt_img_wide = np.zeros_like(img_shape)
        
        # x and y coordinates for the predicted lane
        right_x_coords = np.array([coord[0] for coord in right_pred_line])
        right_y_hat_coords = np.array([coord[1] for coord in right_pred_line])
        right_y_coords = np.array([coord[1] for coord in right_labels])
        for i in range(len(right_x_coords) - 1):
            right_gt_img_wide = cv2.line(right_gt_img_wide, (int(right_x_coords[i]), int(right_y_coords[i])), (int(right_x_coords[i + 1]), int(right_y_coords[i + 1])), (255, 255, 255), self.f1_threshold)
            right_gt_img = cv2.line(right_gt_img, (int(right_x_coords[i]), int(right_y_coords[i])), (int(right_x_coords[i + 1]), int(right_y_coords[i + 1])), (255, 255, 255), 1)
            predicted = cv2.line(right_predicted, (int(right_x_coords[i]), int(right_y_hat_coords[i])), (int(right_x_coords[i + 1]), int(right_y_hat_coords[i + 1])), (255, 255, 255), 1)
            right_predicted_wide = cv2.line(right_predicted_wide, (int(right_x_coords[i]), int(right_y_hat_coords[i])), (int(right_x_coords[i + 1]), int(right_y_hat_coords[i + 1])), (255, 255, 255), self.f1_threshold)
        
        
        # Merge the left and right images
        predicted = np.maximum(left_predicted, right_predicted)
        gt_img = np.maximum(left_gt_img, right_gt_img)
        predicted_wide = np.maximum(left_predicted_wide, right_predicted_wide)
        gt_img_wide = np.maximum(left_gt_img_wide, right_gt_img_wide)
        # Merge the left and right coordinates
        x_coords = np.concatenate((left_x_coords, right_x_coords))
        y_hat_coords = np.concatenate((left_y_hat_coords, right_y_hat_coords))
        y_coords = np.concatenate((left_y_coords, right_y_coords))

        # Compute the F1 score
        tp = np.sum(np.logical_and(predicted == 255, gt_img_wide == 255))
        fp = np.sum(np.logical_and(predicted == 255, gt_img_wide == 0))
        fn = np.sum(np.logical_and(predicted_wide == 0, gt_img == 255))
        accuracy = tp / (tp + fp + fn)

        # Plot the predicted lane and the label
        fig, ax = plt.subplots(1, 1, figsize=(15, 5))
        F1_out_img = gt_img_wide.copy()
        F1_out_img[np.where((F1_out_img == [255, 255, 255]).all(axis=2))] = [189, 252, 201]
        F1_out_img[np.where((F1_out_img == [0, 0, 0]).all(axis=2))] = [255, 255, 255]
        ax.imshow(F1_out_img)
        ax.plot(left_x_coords, left_y_coords, 'g', label='Ground Truth')
        ax.plot(right_x_coords, right_y_coords, 'g', label='Ground Truth')
        ax.plot(left_x_coords, left_y_hat_coords, 'r', label='Line Prediction')
        ax.plot(right_x_coords, right_y_hat_coords, 'r', label='Line Prediction')
        ax.legend(loc='best')
        ax.set_title('F1 Score: {:.2f}'.format(accuracy))
        ax.axis('off')
        return fig
    
    def render_LR_RANSAC(self, left_ransac_line, right_ransac_line, left_model, right_model, left_pixels, right_pixels):
        """
        Renders the left and right RANSAC line in one image.
        """
        # Compute the inlier and outlier maks
        left_outlier_mask = left_model.predict(np.expand_dims(np.array([coord[0] for coord in left_pixels]), axis=1)) - np.array([coord[1] for coord in left_pixels]) >= self.ransac_residual_threshold
        left_outlier_mask += left_model.predict(np.expand_dims(np.array([coord[0] for coord in left_pixels]), axis=1)) - np.array([coord[1] for coord in left_pixels]) <= -self.ransac_residual_threshold
        left_inlier_mask = np.logical_not(left_outlier_mask)
        right_outlier_mask = right_model.predict(np.expand_dims(np.array([coord[0] for coord in right_pixels]), axis=1)) - np.array([coord[1] for coord in right_pixels]) >= self.ransac_residual_threshold
        right_outlier_mask += right_model.predict(np.expand_dims(np.array([coord[0] for coord in right_pixels]), axis=1)) - np.array([coord[1] for coord in right_pixels]) <= -self.ransac_residual_threshold
        right_inlier_mask = np.logical_not(right_outlier_mask)

        # Get the line coordinates
        left_x_pixels = np.array([coord[0] for coord in left_pixels])
        left_y_pixels = np.array([coord[1] for coord in left_pixels])
        right_y_pixels = np.array([coord[1] for coord in right_pixels])
        right_x_pixels = np.array([coord[0] for coord in right_pixels])
        left_ransac_x = np.array([coord[0] for coord in left_ransac_line])
        left_ransac_y = np.array([coord[1] for coord in left_ransac_line])
        right_ransac_x = np.array([coord[0] for coord in right_ransac_line])
        right_ransac_y = np.array([coord[1] for coord in right_ransac_line])

        # Plot the RANSAC result
        fig, ax = plt.subplots(1, 1, figsize=(15, 5))
        ax.scatter(left_x_pixels[left_inlier_mask], left_y_pixels[left_inlier_mask], color='yellowgreen', marker='.', label='Inliers')
        ax.scatter(left_x_pixels[left_outlier_mask], left_y_pixels[left_outlier_mask], color=(1, 0.4, 0.4), marker='.', label='Outliers', s=0.05)
        ax.scatter(right_x_pixels[right_inlier_mask], right_y_pixels[right_inlier_mask], color='yellowgreen', marker='.')
        ax.scatter(right_x_pixels[right_outlier_mask], right_y_pixels[right_outlier_mask], color=(1, 0.4, 0.4), marker='.', s=0.05)
        ax.plot(left_ransac_x, left_ransac_y, color='cornflowerblue', linewidth=2, label='RANSAC regressor')
        ax.plot(right_ransac_x, right_ransac_y, color='cornflowerblue', linewidth=2)
        ax.fill_between(left_ransac_x, left_ransac_y - self.ransac_residual_threshold, left_ransac_y + self.ransac_residual_threshold, alpha=0.2, color='cornflowerblue', label='Residual threshold')
        ax.fill_between(right_ransac_x, right_ransac_y - self.ransac_residual_threshold, right_ransac_y + self.ransac_residual_threshold, alpha=0.2, color='cornflowerblue')
        ax.set_title('RANSAC regressor with {} inliers'.format(len(left_x_pixels[left_inlier_mask]) + len(right_x_pixels[right_inlier_mask])))
        ax.legend(loc='best')
        ax.axis('off')
        return fig
               
    def render_RANSAC(self, ransac_line, pred_coords, label_coords, pixels, model, img_shape):
        """
        Renders the RANSAC line on the image.
        """
        # Check prediction and label coordinates are the same shape
        assert len(pred_coords) == len(label_coords)
        # Draw the predicted lane and label
        predicted = np.zeros_like(img_shape)
        gt_img = np.zeros_like(img_shape)
        predicted_wide = np.zeros_like(img_shape)
        gt_img_wide = np.zeros_like(img_shape)
        # x and y coordinates for the predicted lane
        x_coords = np.array([coord[0] for coord in pred_coords])
        y_hat_coords = np.array([coord[1] for coord in pred_coords])
        y_coords = np.array([coord[1] for coord in label_coords])
        for i in range(len(x_coords) - 1):
            gt_img_wide = cv2.line(gt_img_wide, (int(x_coords[i]), int(y_coords[i])), (int(x_coords[i + 1]), int(y_coords[i + 1])), (255, 255, 255), self.f1_threshold)
            gt_img = cv2.line(gt_img, (int(x_coords[i]), int(y_coords[i])), (int(x_coords[i + 1]), int(y_coords[i + 1])), (255, 255, 255), 1)
            predicted = cv2.line(predicted, (int(x_coords[i]), int(y_hat_coords[i])), (int(x_coords[i + 1]), int(y_hat_coords[i + 1])), (255, 255, 255), 1)
            predicted_wide = cv2.line(predicted_wide, (int(x_coords[i]), int(y_hat_coords[i])), (int(x_coords[i + 1]), int(y_hat_coords[i + 1])), (255, 255, 255), self.f1_threshold)
        # Compute the F1 score
        tp = np.sum(np.logical_and(predicted == 255, gt_img_wide == 255))
        fp = np.sum(np.logical_and(predicted == 255, gt_img_wide == 0))
        fn = np.sum(np.logical_and(predicted_wide == 0, gt_img == 255))
        accuracy = tp / (tp + fp + fn)
        # Compute the inlier and outlier masks
        outlier_mask = model.predict(np.expand_dims(np.array([coord[0] for coord in pixels]), axis=1)) - np.array([coord[1] for coord in pixels]) >= self.ransac_residual_threshold
        outlier_mask += model.predict(np.expand_dims(np.array([coord[0] for coord in pixels]), axis=1)) - np.array([coord[1] for coord in pixels]) <= -self.ransac_residual_threshold
        inlier_mask = np.logical_not(outlier_mask)
        #outlier_mask = np.logical_not(inlier_mask)
        # Get the line coordinates
        x_pixels = np.array([coord[0] for coord in pixels])
        y_pixels = np.array([coord[1] for coord in pixels])
        x_coords = np.array([coord[0] for coord in pred_coords])
        y_hat_coords = np.array([coord[1] for coord in pred_coords])
        y_coords = np.array([coord[1] for coord in label_coords]) 
        ransac_x = np.array([coord[0] for coord in ransac_line])
        ransac_y = np.array([coord[1] for coord in ransac_line])
        # Plot the RANSAC result and the F1 score
        fig, ax = plt.subplots(1, 2, figsize=(15, 5))
        ax[0].scatter(x_pixels[inlier_mask], y_pixels[inlier_mask], color='yellowgreen', marker='.', label='Inliers')
        ax[0].scatter(x_pixels[outlier_mask], y_pixels[outlier_mask], color=(1, 0.4, 0.4), marker='.', label='Outliers', s=0.05)
        ax[0].plot(ransac_x, ransac_y, color='cornflowerblue', linewidth=2, label='RANSAC regressor')
        ax[0].fill_between(ransac_x, ransac_y - self.ransac_residual_threshold, ransac_y + self.ransac_residual_threshold, alpha=0.2, color='cornflowerblue', label='Residual threshold')
        ax[0].set_title('RANSAC regressor, F1 score: {:.2f}'.format(accuracy))
        ax[0].legend(loc='best')
        ax[0].axis('off')
        # Plot the predicted lane and the label
        F1_out_img = gt_img_wide.copy()
        F1_out_img[np.where((F1_out_img == [255, 255, 255]).all(axis=2))] = [189, 252, 201]
        F1_out_img[np.where((F1_out_img == [0, 0, 0]).all(axis=2))] = [255, 255, 255]
        ax[1].imshow(F1_out_img)
        ax[1].imshow(F1_out_img)
        ax[1].plot(x_coords, y_coords, 'g', label='Ground Truth')
        ax[1].plot(x_coords, y_hat_coords, 'r', label='Line Prediction')
        ax[1].legend(loc='best')
        ax[1].set_title('F1 Score: {:.2f}'.format(accuracy))
        ax[1].axis('off')
        return fig
            
    async def main(self, args):
        if len(args) < 2:
            print("Usage: python QuickFit.py <path_to_images> <path_to_labels> <path_to_output>")
            return
        # Create a dictionary to store the average F1 scores and image paths
        avg_scores = []
        # Loop through the images and labels
        for n, image_path in enumerate(glob.glob(args[0]+"**/20.jpg")):
            # T=0
            start_time = time.time()
            # Load the image
            t_0 = time.time()
            image = self.load_image(image_path)
            # Load the label
            clip = '/'.join(image_path.split("/")[-4:-1])+'/20.jpg'
            label = self.load_labels(args[1], clip)
            load_dt = time.time() - t_0
            # Apply a filter to the image
            t_0 = time.time()
            pixels = self.apply_filter(image)
            # Split the pixels into left and right halves
            left, right = self.split_pixels(pixels, image)
            filter_dt = time.time() - t_0
            # Fit a line to the left and right halves
            t_0 = time.time()
            with ProcessPoolExecutor(max_workers=2) as executor:
                left_model = executor.submit(self.fit_line, left)
                right_model = executor.submit(self.fit_line, right)
                left_model = left_model.result()
                right_model = right_model.result()
            ransac_dt = time.time() - t_0
            # Predict the left and right lines
            t_0 = time.time()
            left_lane, left_pred_lane = self.get_best_MSE(label, left_model)
            right_lane, right_pred_lane = self.get_best_MSE(label, right_model)
            total_time = time.time() - start_time
            pred_dt = time.time() - t_0
            # Compute the F1 score
            t_0 = time.time()
            left_score, _, _ = self.compute_f1_score(left_lane, left_pred_lane, image)
            right_score, _, _ = self.compute_f1_score(right_lane, right_pred_lane, image)
            avg_score = (left_score + right_score) / 2
            f1_dt = time.time() - t_0
            # Show progress
            if n % 10 == 0:
                print("Processing image {} of {}".format(n, len(glob.glob(args[0]+"**/20.jpg"))))
                # Print performance stats
                print("F1: {:.2f} | Total time: {:.2f}s | RANSAC: {:.2f}% | load: {:.2f}% | prediction: {:.2f}% | filter: {:.2f}%".format(avg_score, total_time, ransac_dt/total_time*100, load_dt/total_time*100, pred_dt/total_time*100, filter_dt/total_time*100))
            # Compute the average F1 score
            avg_scores.append({ "image": image_path, "label": args[1], "score": (left_score + right_score) / 2, "time": total_time })
        # Write the average F1 scores to a csv file
        with open(args[2], "w") as f:
            # Write the header
            f.write("image, score, execution_time\n")
            for score in avg_scores:
                line = "{}, {}, {}\n".format(score["image"], score["score"], score["time"])
                f.write(line)
        

if __name__ == "__main__":
    # Create a new instance of the QuickFit class
    qf = QuickFit(ransac_max_trials=10, ransac_random_state=42, polynomial_degree=1)
    # Run the main function
    loop = asyncio.get_event_loop()
    loop.run_until_complete(qf.main(sys.argv[1:]))
